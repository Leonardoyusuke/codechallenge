import express from 'express';
import cors from 'cors';
import { valuesRouter } from './routers/values-router.js';

const app = express();
app.use(cors())
.use(express.json())
.get('/health', (req, res) => res.send('OK!'))
.use('/values', valuesRouter)


const PORT = 5001;
app.listen(PORT, () => console.log(`Rodando na porta ${PORT}`));
