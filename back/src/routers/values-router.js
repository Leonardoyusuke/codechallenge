import { Router } from "express";
import valuesController from "../controllers/values-controllers.js";

const valuesRouter = Router();

valuesRouter.post('/', valuesController.values);

export {valuesRouter}