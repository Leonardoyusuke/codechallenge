import valuesService from './values-services.js';
const values = valuesService.values;

describe('values', () => {
  test('should throw an error if any wall size is less than 1 or greater than 50', () => {
    const payload = {
      total1: 0,
      total2: 10,
      total3: 20,
      total4: 60,
    };

    expect(() => values(payload)).toThrow('Todas paredes devem ter no minimo 1 metro quadrado e no maximo 50 metros quadrados');
  });

  test('should throw an error if doors and windows occupy more than 50% of the wall', () => {
    const payload = {
      total1: 10,
      total2: 20,
      total3: 30,
      total4: 40,
      porta1: 5,
      janela1: 5,
      porta2: 10,
      janela2: 10,
      porta3: 15,
      janela3: 15,
      porta4: 20,
      janela4: 20,
    };

    expect(() => values(payload)).toThrow('Portas e janelas não podem ocupar mais de 50% da parede');
  });

  test('should throw an error if the door height is less than 2.20 and the number of doors is greater than 0', () => {
    const payload = {
      total1: 10,
      total2: 20,
      total3: 30,
      total4: 40,
      porta1: 5,
      altura1: 2.10,
    };

    expect(() => values(payload)).toThrow('Altura minima para porta é 2.20 metros quadrados');
  });

  test('should calculate the required paint cans', () => {
    const payload = {
      total1: 22,
      total2: 22,
      total3: 22,
      total4: 22,
      porta1: 2,
      janela1: 1,
      porta2: 1,
      janela2: 1,
      porta3: 0,
      janela3: 1,
      porta4: 0,
      janela4: 2,
      altura1: 2.20,
      altura2: 2.20,
      altura3: 2.20,
      altura4: 2.20,
      totalPorta: 3,
      totalJanela: 5,
    };

    const expected = [
      { size: 3.6, quantity: 3 },
      { size: 2.5, quantity: 1 },
      { size: 0.5, quantity: 2}
    ];

    expect(values(payload)).toEqual(expected);
  });
});