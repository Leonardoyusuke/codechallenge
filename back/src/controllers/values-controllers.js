import httpStatus from 'http-status'
import valuesService from '../services/values-services.js'

async function values(req, res) {
    const payload = req.body
    try {
        const value = valuesService.values(payload)
        return res.status(httpStatus.OK).json(value)
    } catch (error) {
        console.log(error)
        if (error.message === 'Todas paredes devem ter no minimo 1 metro quadrado e no maximo 50 metros quadrados') {
            return res.status(httpStatus.BAD_REQUEST).json({ message: error.message })
        }
        if (error.message === 'Portas e janelas não podem ocupar mais de 50% da parede') {
            return res.status(httpStatus.BAD_REQUEST).json({ message: error.message })
        }
        if (error.message === 'Altura minima para porta é 2.20 metros quadrados') {
            return res.status(httpStatus.BAD_REQUEST).json({ message: error.message })
        }
    }
}

const valuesController = {
    values,
}

export default valuesController