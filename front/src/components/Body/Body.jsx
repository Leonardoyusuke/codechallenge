import './Body.css';
import { useState, useEffect } from 'react';
import axios from 'axios';

export default function Body() {
    const [altura1, setAltura1] = useState(0);
    const [largura1, setLargura1] = useState(0);
    const [total1, setTotal1] = useState(0);
    const [altura2, setAltura2] = useState(0);
    const [largura2, setLargura2] = useState(0);
    const [total2, setTotal2] = useState(0);
    const [altura3, setAltura3] = useState(0);
    const [largura3, setLargura3] = useState(0);
    const [total3, setTotal3] = useState(0);
    const [altura4, setAltura4] = useState(0);
    const [largura4, setLargura4] = useState(0);
    const [total4, setTotal4] = useState(0);
    const [total, setTotal] = useState(0);

    const [porta1, setPorta1] = useState(0);
    const [janela1, setJanela1] = useState(0);
    const [porta2, setPorta2] = useState(0);
    const [janela2, setJanela2] = useState(0);
    const [porta3, setPorta3] = useState(0);
    const [janela3, setJanela3] = useState(0);
    const [porta4, setPorta4] = useState(0);
    const [janela4, setJanela4] = useState(0);
    const [totalPorta, setTotalPorta] = useState(0);
    const [totalJanela, setTotalJanela] = useState(0);
    const [tinta, setTinta] = useState("");
 
    useEffect(() => {
        setTotal1(altura1 * largura1);
        setTotal2(altura2 * largura2);
        setTotal3(altura3 * largura3);
        setTotal4(altura4 * largura4);
        setTotalPorta(porta1 + porta2 + porta3 + porta4);
        setTotalJanela(janela1 + janela2 + janela3 + janela4);
    }, [altura1, largura1, altura2, largura2, altura3, largura3, altura4, largura4,
        porta1, porta2, porta3, porta4, janela1, janela2, janela3, janela4]);

    useEffect(() => {
        setTotal(total1 + total2 + total3 + total4);
    }, [total1, total2, total3, total4]);

    const payload = {
        altura1: altura1, largura1: largura1, altura2: altura2, largura2: largura2, altura3: altura3, largura3: largura3,
        altura4: altura4, largura4: largura4, porta1: porta1, janela1: janela1, porta2: porta2, janela2: janela2,
        porta3: porta3, janela3: janela3, porta4: porta4, janela4: janela4, total1: total1, total2: total2,
        total3: total3, total4: total4, total: total, totalPorta: totalPorta, totalJanela: totalJanela
    };

    async function calcular(e) {
        e.preventDefault();
        const response = await axios.post('http://localhost:5001/values', payload);
        setTinta(response.data);

    }

    return (
        <div className="container">
            <h1>1º coloque as medidas das suas paredes, cada parede tem que ter um minimo de 1 metro quadrado e no maximo 50 metros quadrados </h1>
            <h1>2º Se houver janelas ou portas, adicione-as, para adicionar portas a sua parede tera que ter uma altura minima de 2.20m </h1>
            <h1>3º Cada porta mede 1.90m de altura e 0.80m de largura, cada janela mede 2m de largura e 1.2m de altura</h1>
            <h1>4º O total de área das portas e janelas deve ser no máximo 50% da área de parede</h1>
            <form className='form' onSubmit={calcular}>
                <div className='div'>
                    <h1>Parede 1</h1>
                    <input step=".1" type="number" onChange={(e) => setAltura1(parseFloat(e.target.value))} placeholder="Altura em metros" required />
                    <input step=".1" type="number" onChange={(e) => setLargura1(parseFloat(e.target.value))} placeholder="Largura em metros" required />
                    <button type="button" onClick={() => setJanela1(janela1 + 1)} className='buttonAdd' disabled={(altura1 < 1.2 && largura1 < 2) || (porta1 * 1.52 + janela1 * 2.4) > total1 / 2 - 2.4}>
                        adicionar janela
                    </button>
                    <button type="button" onClick={() => setPorta1(porta1 + 1)} className='buttonAdd' disabled={(altura1 < 2.2 && largura1 < 0.8) || (porta1 * 1.52 + janela1 * 2.4) > total1 / 2 - 1.52}>
                        adicionar porta
                    </button>
                    {total1 !== 0 ? <h2>Total: {total1} metros quadrados com {porta1} portas e {janela1} janelas </h2> : ""}
                </div>
                <div className='div'>
                    <h1>Parede 2</h1>
                    <input step=".1" type="number" onChange={(e) => setAltura2(parseFloat(e.target.value))} placeholder="Altura em metros" required />
                    <input step=".1" type="number" onChange={(e) => setLargura2(parseFloat(e.target.value))} placeholder="Largura em metros" required />
                    <button type="button" onClick={() => setJanela2(janela2 + 1)} className='buttonAdd' disabled={(altura2 < 1.2 && largura2 < 2) || (porta2 * 1.52 + janela2 * 2.4) > total2 / 2 - 2.4}>
                        adicionar janela
                    </button>
                    <button type="button" onClick={() => setPorta2(porta2 + 1)} className='buttonAdd' disabled={(altura2 < 2.2 && largura2 < 0.8) || (porta2 * 1.52 + janela2 * 2.4) > total2 / 2 - 1.52}>
                        adicionar porta
                    </button>
                    {total2 !== 0 ? <h2>Total: {total2} metros quadrados com {porta2} portas e {janela2} janelas</h2> : ""}
                </div>
                <div className='div'>
                    <h1>Parede 3</h1>
                    <input step=".1" type="number" onChange={(e) => setAltura3(parseFloat(e.target.value))} placeholder="Altura em metros" required />
                    <input step=".1" type="number" onChange={(e) => setLargura3(parseFloat(e.target.value))} placeholder="Largura em metros" required />
                    <button type="button" onClick={() => setJanela3(janela3 + 1)} className='buttonAdd' disabled={(altura3 < 1.2 && largura3 < 2) || (porta3 * 1.52 + janela3 * 2.4) > total3 / 2 - 2.4}>
                        adicionar janela
                    </button>
                    <button type="button" onClick={() => setPorta3(porta3 + 1)} className='buttonAdd' disabled={(altura3 < 2.2 && largura3 < 0.8) || (porta3 * 1.52 + janela3 * 2.4) > total3 / 2 - 1.52}>
                        adicionar porta
                    </button>
                    {total3 !== 0 ? <h2>Total: {total3} metros quadrados com {porta3} portas e {janela3} janelas</h2> : ""}

                </div>
                <div className='div'>
                    <h1>Parede 4</h1>
                    <input step=".1" type="number" onChange={(e) => setAltura4(parseFloat(e.target.value))} placeholder="Altura em metros" required />
                    <input step=".1" type="number" onChange={(e) => setLargura4(parseFloat(e.target.value))} placeholder="Largura em metros" required />
                    <button type="button" onClick={() => setJanela4(janela4 + 1)} className='buttonAdd' disabled={(altura4 < 1.2 && largura4 < 2) || (porta4 * 1.52 + janela4 * 2.4) > total4 / 2 - 2.4}>
                        adicionar janela
                    </button>
                    <button type="button" onClick={() => setPorta4(porta4 + 1)} className='buttonAdd' disabled={(altura4 < 2.2 && largura4 < 0.8) || (porta4 * 1.52 + janela4 * 2.4) > total4 / 2 - 1.52}>
                        adicionar porta
                    </button>
                    {total4 !== 0 ? <h2>Total: {total4} metros quadrados com {porta4} portas e {janela4} janelas</h2> : ""}
                </div>
                <button type="submit" className='buttonSubmit'>Calcular</button>
            </form>
            {total !== 0 ? <h1>Total: {total} metros quadrados com {totalPorta} portas e {totalJanela} janelas  </h1> : ""}
           <div className='resultado'>
            {tinta? <h1>Voce vai precisar de: </h1>:""}
            {tinta? tinta.map((tinta, index) => (
                <p key={index}>  { tinta.quantity} latas de {tinta.size}L, </p>
            )
            ):""}
            </div> 
        </div>
    );
}
