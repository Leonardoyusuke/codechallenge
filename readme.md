# Sem Desperdício

Seu app para não desperdiçar tinta.

## Requisitos

- Docker
- Docker Compose 

## Como Usar

### Passos Iniciais

1. Clone o repositório:

    ```sh
    git clone https://gitlab.com/Leonardoyusuke/codechallenge
    cd codechallenge
    ```

### Usando Docker Compose

1. Certifique-se de ter o Docker Compose instalado.
2. Inicie os serviços definidos no `docker-compose.yml`:

    ```sh
    docker-compose up -d
    ```

3. Para parar os serviços:

    ```sh
    docker-compose down
    ```

### Acessando o Projeto

Depois de executar os serviços com Docker Compose, o projeto estará disponível em `http://localhost:3000`.

## Estrutura do Projeto

- `Dockerfile`: Define a imagem Docker.
- `docker-compose.yml`: Arquivo de configuração do Docker Compose.
- `src/`: Código-fonte do projeto.
- `README.md`: Este arquivo.

## Contribuindo

1. Faça um fork do projeto.
2. Crie um branch para sua feature (`git checkout -b feature/nova-feature`).
3. Commit suas mudanças (`git commit -m 'Adiciona nova feature'`).
4. Faça o push para o branch (`git push origin feature/nova-feature`).
5. Abra um Pull Request.