function values(payload) {
    if (payload.total1 < 1 || payload.total1 > 50 ||
        payload.total2 < 1 || payload.total2 > 50 ||
        payload.total3 < 1 || payload.total3 > 50 ||
        payload.total4 < 1 || payload.total4 > 50) {
        throw new Error('Todas paredes devem ter no minimo 1 metro quadrado e no maximo 50 metros quadrados');
    }

    if (payload.porta1 * 1.52 + payload.janela1 * 2.4 > payload.total1/2 ||
        payload.porta2 * 1.52 + payload.janela2 * 2.4 > payload.total2/2 ||
        payload.porta3 * 1.52 + payload.janela3 * 2.4 > payload.total3/2 ||
        payload.porta4 * 1.52 + payload.janela4 * 2.4 > payload.total4/2 ) {
        throw new Error('Portas e janelas não podem ocupar mais de 50% da parede');
    }

    if (payload.altura1 < 2.20 && payload.porta1 > 0 ||
        payload.altura2 < 2.20 && payload.porta2 > 0 ||
        payload.altura3 < 2.20 && payload.porta3 > 0 ||
        payload.altura4 < 2.20 && payload.porta4 > 0) {
        throw new Error('Altura minima para porta é 2.20 metros quadrados');
    }

    const totalArea = payload.total1 + payload.total2 + payload.total3 + payload.total4;
    const wallPaint = totalArea - (payload.totalPorta * 1.52) - (payload.totalJanela * 2.4);
    let literPaint = wallPaint / 5;
    console.log(literPaint)

    const canSizes = [18, 3.6, 2.5, 0.5];
    let remainingLiters = literPaint;
    const result = [];

    for(let i = 0; remainingLiters > 0; i++) {
        const canSize = canSizes[i];
        const cans = Math.floor(remainingLiters / canSize);
        remainingLiters = remainingLiters % canSize;
        if (cans > 0) {
            result.push({ size: canSize, quantity: cans });
        }
        if(remainingLiters > 0 && remainingLiters < 0.5){
            const halfLiterCan = result.find(entry => entry.size === 0.5);
            if (halfLiterCan) {
                halfLiterCan.quantity++;
            } else {
                result.push({ size: 0.5, quantity: 1 });
            }
            break;
        }
    }
    return result;
}

const valuesService = {
    values,
};

export default valuesService;
